package com.aait.allabnahclient.Network

import com.aait.allabnahclient.Models.*
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part
import retrofit2.http.Query

interface Service {
    @POST("about")
    fun About(@Query("lang") lang:String):Call<TermsResponse>

    @POST("terms")
    fun Terms(@Query("lang") lang: String):Call<TermsResponse>
    @POST("terms-sale")
    fun TermsSale(@Query("lang") lang: String):Call<TermsResponse>

    @POST("contact-us")
    fun ContactUs(@Query("lang") lang: String,
                  @Query("name") name:String,
                  @Query("phone") phone:String,
                  @Query("email") email:String,
                  @Query("message") message:String):Call<TermsResponse>

    @POST("follow-us")
    fun FollowUs():Call<SocialResponse>

    @POST("questions")
    fun Questions(@Query("lang") lang: String):Call<QuestionResponse>



    @POST("details-order-delegate")
    fun Accept(@Query("lang") lang:String,
                     @Query("order_id") order_id:Int,
                     @Query("delegate_id") delegate_id:Int,
                     @Query("action") action:String,
                     @Query("confirm") confirm:Int):Call<BaseResponse>

    @POST("sign-up-user")
    fun SignUp(@Query("phone") phone:String,
               @Query("name") name:String,
               @Query("email") email:String,
               @Query("address") address:String,
               @Query("lat") lat:String,
               @Query("lng") lng:String,
               @Query("password") password:String,
               @Query("device_id") device_id:String,
               @Query("device_type") device_type:String):Call<UserResponse>
    @POST("check-code")
    fun CheckCode(@Query("user_id") user_id:Int,
                  @Query("code") code:String,
                  @Query("lang") lang:String):Call<UserResponse>
    @POST("resend-code")
    fun ReSend(@Query("user_id") user_id:Int):Call<TermsResponse>

    @POST("sign-in")
    fun Login(@Query("lang") lang: String,
              @Query("phone") phone:String,
              @Query("password") password:String,
              @Query("device_id") device_id:String,
              @Query("device_type") device_type:String):Call<UserResponse>



    @POST("edit-profile")
    fun getProfile(@Query("user_id") user_id: Int):Call<UserResponse>
    @Multipart
    @POST("edit-profile")
    fun UpdateImage(@Query("user_id") user_id:Int,
                    @Query("lang") lang: String,
                    @Part avater:MultipartBody.Part):Call<UserResponse>

    @POST("edit-profile")
    fun update(@Query("user_id") user_id:Int,
               @Query("lang") lang: String,
               @Query("name") name:String,
               @Query("address") address:String,
               @Query("lat") lat:String,
               @Query("lng") lng: String,
               @Query("phone") phone:String,
               @Query("email") email: String):Call<UserResponse>
    @POST("reset-password")
    fun resetPassword(@Query("lang") lang:String,
                      @Query("user_id") user_id: Int,
                      @Query("current_password") current_password:String,
                      @Query("password") password:String):Call<BaseResponse>

    @POST("contact-whatsapp")
    fun WhatsApp():Call<TermsResponse>
    @POST("forget-password")
    fun ForGot(@Query("phone") phone:String,
               @Query("lang") lang:String):Call<UserResponse>

    @POST("update-password")
    fun NewPass(@Query("user_id") user_id: Int,
                @Query("password") password:String,
                @Query("code") code:String,
                @Query("lang") lang: String):Call<UserResponse>
    @POST("log-out")
    fun logOut(@Query("user_id") user_id:Int,
               @Query("device_id") device_id:String,
               @Query("device_type") device_type:String,
               @Query("lang") lang:String):Call<TermsResponse>

    @POST("notifications")
    fun Notifications(@Query("user_id") user_id: Int,
                      @Query("lang") lang: String):Call<NotificationResponse>
    @POST("delete-notification")
    fun delete(
        @Query("user_id") user_id:Int,
        @Query("notification_id") notification_id:Int,
        @Query("lang") lang: String):Call<TermsResponse>

    @POST("categories")
    fun Categories(@Query("lang") lang: String):Call<CategoryResponse>

    @POST("subcategories")
    fun SubCategory(@Query("lang") lang: String,
                    @Query("category_id") category_id:Int):Call<SubCategoryResponse>

    @POST("user-balance")
    fun Balance(@Query("lang") lang: String,
                @Query("user_id") user_id: Int):Call<TermsResponse>

    @POST("favorite")
    fun AddFav(@Query("lang") lang: String,
               @Query("user_id") user_id: Int,
               @Query("subcategory_id") subcategory_id:Int):Call<FavResponse>

    @POST("details-subcategory")
    fun Details(@Query("lang") lang: String,
                @Query("subcategory_id") subcategory_id:Int,
                @Query("user_id") user_id:Int?):Call<SubCategoryDetailsResponse>

    @POST("my-favorites")
    fun MyFavs(@Query("lang") lang:String,
               @Query("user_id") user_id:Int):Call<FavsResponse>

    @POST("add-order")
    fun AddOrder(@Query("lang") lang: String,
                 @Query("user_id") user_id: Int,
                 @Query("carts") carts:String,
                 @Query("city_id") city_id:Int,
                 @Query("region_id") region_id:Int,
                 @Query("color_id") color_id:Int?,
                 @Query("street") street:String,
                 @Query("recipient_name") recipient_name:String,
                 @Query("phone") phone:String,
                 @Query("lat") lat:String,
                 @Query("lng") lng: String,
                 @Query("address") address:String,
                 @Query("delivery_price") delivery_price:String,
                 @Query("tax") tax:String,
                 @Query("total") total:String,
                 @Query("car_numbers") car_numbers:Int,
                 @Query("final_total") final_total:String):Call<FavResponse>

    @POST("costs")
    fun Coast():Call<CoastResponse>
    @POST("cities")
    fun Cities(@Query("lang") lang:String):Call<ListResponse>

    @POST("regions")
    fun Regions(@Query("lang") lang: String,
                @Query("city_id") city_id:Int):Call<ListResponse>

    @POST("user-orders")
    fun Orders(@Query("lang") lang:String,
               @Query("user_id") user_id: Int,
               @Query("orders_type") orders_type:String):Call<OrdersResponse>
    @Multipart
    @POST("payment-transfer")
    fun Pay(@Query("lang") lang: String,
            @Query("user_id") user_id: Int,
            @Query("order_id") order_id: Int,
            @Query("action") action:Int,
            @Query("total") total:String,
            @Query("bank_name") bank_name:String,
            @Query("account_name") account_name:String,
            @Part image:MultipartBody.Part):Call<BaseResponse>

    @POST("details-order")
    fun Detailsorder(@Query("lang") lang:String,
                @Query("user_id") user_id: Int,
                @Query("order_id") order_id: Int,
                @Query("user_type") user_type:String):Call<OrderDetailsResponse>

    @POST("app-screens")
    fun Sreens(@Query("lang") lang: String):Call<ScreenResponse>

    @POST("request-service")
    fun RequestService(@Query("lang") lang:String,
                       @Query("user_id") user_id:Int,
                       @Query("subject") subject:String,
                       @Query("details") details:String,
                       @Query("service_type") service_type:String,
                       @Query("service_details") service_details:String?):Call<TermsResponse>

    @POST("services")
    fun Services(@Query("lang") lang: String):Call<ServiceResponse>
}