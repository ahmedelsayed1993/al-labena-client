package com.aait.allabnahclient.UI.Activities

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.viewpager.widget.ViewPager
import com.aait.allabnahclient.Base.Parent_Activity
import com.aait.allabnahclient.R
import com.aait.allabnahclient.UI.Adapters.SubscribeTapAdapter
import com.google.android.material.tabs.TabLayout

class OrdersActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_orders
    lateinit var orders: TabLayout
    lateinit var ordersViewPager: ViewPager
    private var mAdapter: SubscribeTapAdapter? = null

    lateinit var title: TextView
    lateinit var back: ImageView

    override fun initializeComponents() {
        title = findViewById(R.id.title)
        back = findViewById(R.id.back)
        back.setOnClickListener { onBackPressed()
        finish()}
        title.text = getString(R.string.orders)
        orders = findViewById(R.id.orders)
        ordersViewPager = findViewById(R.id.ordersViewPager)
        mAdapter = SubscribeTapAdapter(mContext!!,supportFragmentManager)
        ordersViewPager.setAdapter(mAdapter)
        orders!!.setupWithViewPager(ordersViewPager)
    }
}