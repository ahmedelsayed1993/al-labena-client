package com.aait.allabnahclient.UI.Activities

import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.allabnahclient.Base.Parent_Activity
import com.aait.allabnahclient.Client
import com.aait.allabnahclient.Listeners.OnItemClickListener
import com.aait.allabnahclient.Models.*
import com.aait.allabnahclient.Network.Service
import com.aait.allabnahclient.R
import com.aait.allabnahclient.UI.Adapters.ServiceAdapter
import com.aait.allabnahclient.UI.Adapters.SubCategoryAdapter
import com.aait.allabnahclient.Uitls.CommonUtil
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ServicesActivity:Parent_Activity(),OnItemClickListener {
    override fun onItemClick(view: View, position: Int) {
        val intent = Intent(this,RequestServicesActivity::class.java)
        intent.putExtra("name",subCategorymodels.get(position).name)
        intent.putExtra("type",subCategorymodels.get(position).type)
        startActivity(intent)

    }

    override val layoutResource: Int
        get() = R.layout.activity_services
    lateinit var title: TextView
    lateinit var back: ImageView
    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null

    internal var layNoItem: RelativeLayout? = null

    internal var tvNoContent: TextView? = null

    var swipeRefresh: SwipeRefreshLayout? = null
    lateinit var gridLayoutManager: LinearLayoutManager
    var subCategorymodels=ArrayList<ServiceModel>()
    lateinit var subCategoryAdapter: ServiceAdapter

    override fun initializeComponents() {

        title = findViewById(R.id.title)
        back = findViewById(R.id.back)
        rv_recycle = findViewById(R.id.rv_recycle)
        layNoInternet = findViewById(R.id.lay_no_internet)
        layNoItem = findViewById(R.id.lay_no_item)
        tvNoContent = findViewById(R.id.tv_no_content)
        swipeRefresh = findViewById(R.id.swipe_refresh)
        title.text = getString(R.string.services)
        back.setOnClickListener { onBackPressed() }
        gridLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        subCategoryAdapter = ServiceAdapter(mContext,subCategorymodels,R.layout.recycler_service)
        subCategoryAdapter.setOnItemClickListener(this)
        rv_recycle.layoutManager = gridLayoutManager
        rv_recycle.adapter = subCategoryAdapter
        swipeRefresh!!.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener {
            getData()

        }
        getData()
    }

    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.Services(mLanguagePrefManager.appLanguage)?.enqueue(object :
            Callback<ServiceResponse> {
            override fun onFailure(call: Call<ServiceResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<ServiceResponse>,
                response: Response<ServiceResponse>
            ) {
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
                if (response.isSuccessful) {
                    if (response.body()?.value.equals("1")) {
                        Log.e("myJobs", Gson().toJson(response.body()!!.data))
                        if (response.body()!!.data?.isEmpty()!!) {
                            layNoItem!!.visibility = View.VISIBLE
                            layNoInternet!!.visibility = View.GONE
                            tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)

                        } else {
//
                            subCategoryAdapter.updateAll(response.body()!!.data!!)
                        }
//
                    }else {
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }

                }
            }
        })
    }
}