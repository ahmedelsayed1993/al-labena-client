package com.aait.allabnahclient.UI.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.aait.allabnahclient.Base.ParentRecyclerAdapter
import com.aait.allabnahclient.Base.ParentRecyclerViewHolder
import com.aait.allabnahclient.Models.QuestionModel
import com.aait.allabnahclient.R
import com.bumptech.glide.Glide

class QuestionsAdapter (context: Context, data: MutableList<QuestionModel>, layoutId: Int) :
    ParentRecyclerAdapter<QuestionModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val questionModel = data.get(position)
        viewHolder.question!!.setText(questionModel.question)
        viewHolder.answer!!.text = questionModel.answer!!

        viewHolder.question_lay.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)
        if (viewHolder.answer.visibility == View.VISIBLE)
        {
            viewHolder.answer.visibility = View.GONE
            viewHolder.down.rotation = 0F
        }else{
            viewHolder.answer.visibility = View.VISIBLE
            viewHolder.down.rotation = 180F
        }
        })
        viewHolder.down.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)
            if (viewHolder.answer.visibility == View.VISIBLE)
            {
                viewHolder.answer.visibility = View.GONE
                viewHolder.down.rotation = 0F
            }else{
                viewHolder.answer.visibility = View.VISIBLE
                viewHolder.down.rotation = 180F
            }
        })



    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {





        internal var question=itemView.findViewById<TextView>(R.id.question)
        internal var down = itemView.findViewById<ImageView>(R.id.down)
        internal var question_lay = itemView.findViewById<LinearLayout>(R.id.question_lay)
        internal var answer = itemView.findViewById<TextView>(R.id.answer)



    }
}