package com.aait.allabnahclient.UI.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.aait.allabnahclient.Base.ParentRecyclerAdapter
import com.aait.allabnahclient.Base.ParentRecyclerViewHolder
import com.aait.allabnahclient.Models.FavModel
import com.aait.allabnahclient.Models.SubCategorymodel
import com.aait.allabnahclient.R
import com.bumptech.glide.Glide

class FavAdapter (context: Context, data: MutableList<FavModel>, layoutId: Int) :
    ParentRecyclerAdapter<FavModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val questionModel = data.get(position)
        viewHolder.name!!.setText(questionModel.name)
        viewHolder.description.text = questionModel.description
        viewHolder.code.text =  questionModel.id.toString()+" # "
        Glide.with(mcontext).load(questionModel.image).into(viewHolder.image)


        viewHolder.itemView.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)

        })






    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {





        internal var image=itemView.findViewById<ImageView>(R.id.image)
        internal var name = itemView.findViewById<TextView>(R.id.name)
        internal var description = itemView.findViewById<TextView>(R.id.description)
        internal var code = itemView.findViewById<TextView>(R.id.code)




    }
}