package com.aait.allabnahclient.UI.Activities

import android.app.Dialog
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.text.InputType
import android.util.Log
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import com.aait.allabnahclient.Base.Parent_Activity
import com.aait.allabnahclient.Client
import com.aait.allabnahclient.Models.TermsResponse
import com.aait.allabnahclient.Models.UserResponse
import com.aait.allabnahclient.Network.Service
import com.aait.allabnahclient.R
import com.aait.allabnahclient.Uitls.CommonUtil
import com.aait.allabnahclient.Uitls.PermissionUtils
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.fxn.utility.ImageQuality
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

class RegisterActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_register
    lateinit var phone:EditText
    lateinit var name:EditText
    lateinit var email:EditText
    lateinit var address:TextView

    lateinit var password:EditText
    lateinit var view:ImageView
    lateinit var confirm_password:EditText
    lateinit var view1:ImageView
    lateinit var forgot_pass:CheckBox
    lateinit var register:Button
    lateinit var login:LinearLayout
    var lat = ""
    var lng = ""
    var location = ""


    var deviceID = ""
    override fun initializeComponents() {
        deviceID = FirebaseInstanceId.getInstance().token.toString()
        phone = findViewById(R.id.phone)
        name = findViewById(R.id.name)
        email = findViewById(R.id.email)
        address = findViewById(R.id.address)

        password = findViewById(R.id.password)
        view = findViewById(R.id.view)
        confirm_password = findViewById(R.id.confirm_password)
        view1 = findViewById(R.id.view1)
        forgot_pass = findViewById(R.id.forgot_pass)
        register = findViewById(R.id.register)
        login = findViewById(R.id.login)
        login.setOnClickListener { startActivity(Intent(this,LoginActivity::class.java)) }
        address.setOnClickListener { startActivityForResult(Intent(this@RegisterActivity,LocationActivity::class.java),1) }
        view.setOnClickListener {  if (password.inputType== InputType.TYPE_TEXT_VARIATION_NORMAL){
            password.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

        }
        else{
            password.inputType = InputType.TYPE_TEXT_VARIATION_NORMAL
        } }
        view1.setOnClickListener {
            if (confirm_password.inputType== InputType.TYPE_TEXT_VARIATION_NORMAL){
                confirm_password.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

            }
            else{
                confirm_password.inputType = InputType.TYPE_TEXT_VARIATION_NORMAL
            }
        }



        register.setOnClickListener {
            if (CommonUtil.checkEditError(phone,getString(R.string.phone_number))||
                    CommonUtil.checkLength1(phone,getString(R.string.phone_length),10)||
                    CommonUtil.checkEditError(name,getString(R.string.enter_name))||
                    CommonUtil.checkEditError(email,getString(R.string.enter_email))||
                    !CommonUtil.isEmailValid(email,getString(R.string.correct_email))||
                    CommonUtil.checkTextError(address,getString(R.string.enter_address))||

                    CommonUtil.checkEditError(password,getString(R.string.enter_password))||
                    CommonUtil.checkLength(password,getString(R.string.password_length),6)||
                    CommonUtil.checkEditError(confirm_password,getString(R.string.enter_password))){
                return@setOnClickListener
            }else{
                if (!password.text.toString().equals(confirm_password.text.toString())){
                    CommonUtil.makeToast(mContext,getString(R.string.password_not_match))
                }else{
                    if (!forgot_pass.isChecked){
                        CommonUtil.makeToast(mContext,getString(R.string.raed_accept))
                    }else{

                        Register()
                    }
                }
            }
        }

        forgot_pass.setOnClickListener {
            val dialog = Dialog(mContext)
            dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog?.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            dialog ?.setCancelable(true)
            dialog ?.setContentView(R.layout.dialog_terms)
            val terms = dialog?.findViewById<TextView>(R.id.terms)
            val agree = dialog?.findViewById<Button>(R.id.agree)

            showProgressDialog(getString(R.string.please_wait))
            Client.getClient()?.create(Service::class.java)?.Terms(mLanguagePrefManager.appLanguage)?.enqueue(object:
                Callback<TermsResponse> {
                override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(call: Call<TermsResponse>, response: Response<TermsResponse>) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            terms.text = response.body()?.data
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

            })
            agree.setOnClickListener { dialog?.dismiss() }




            dialog?.show()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

            if (data?.getStringExtra("result") != null) {
                location = data?.getStringExtra("result").toString()
                lat = data?.getStringExtra("lat").toString()
                lng = data?.getStringExtra("lng").toString()
                address.text = location
            } else {
                location = ""
                lat = ""
                lng = ""
                address.text = ""
            }

    }

    fun Register(){
        showProgressDialog(getString(R.string.please_wait))

        Client.getClient()?.create(Service::class.java)?.SignUp(phone.text.toString(),name.text.toString(),email.text.toString()
        ,address.text.toString(),lat,lng,password.text.toString(),deviceID,"android")
            ?.enqueue(object : Callback<UserResponse> {
                override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                    Log.e("error", Gson().toJson(t))
                }

                override fun onResponse(
                    call: Call<UserResponse>,
                    response: Response<UserResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            val intent = Intent(this@RegisterActivity,ActivateAccountActivity::class.java)
                            intent.putExtra("user",response.body()?.data)
                            startActivity(intent)
                            finish()
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }
            })
    }
}