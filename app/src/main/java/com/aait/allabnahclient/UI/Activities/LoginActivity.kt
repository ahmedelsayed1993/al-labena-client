package com.aait.allabnahclient.UI.Activities

import android.content.Intent
import android.text.InputType
import android.widget.*
import com.aait.allabnahclient.Base.Parent_Activity
import com.aait.allabnahclient.Client
import com.aait.allabnahclient.Models.UserResponse
import com.aait.allabnahclient.Network.Service
import com.aait.allabnahclient.R
import com.aait.allabnahclient.Uitls.CommonUtil
import com.google.firebase.iid.FirebaseInstanceId
import maes.tech.intentanim.CustomIntent
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_login
    lateinit var register:LinearLayout
    lateinit var login:Button
    lateinit var phone:EditText
    lateinit var password:EditText
    lateinit var view:ImageView
    lateinit var visitor:Button
    lateinit var forgot_pass:TextView

    var deviceID = ""
    override fun initializeComponents() {
        deviceID = FirebaseInstanceId.getInstance().token.toString()
        CustomIntent.customType(this,"bottom-to-up")
        register = findViewById(R.id.register)
        login = findViewById(R.id.login)
        phone = findViewById(R.id.phone)
        password = findViewById(R.id.password)
        view = findViewById(R.id.view)
        visitor = findViewById(R.id.visitor)
        forgot_pass = findViewById(R.id.forgot_pass)
        register.setOnClickListener { startActivity(Intent(this,RegisterActivity::class.java)) }
        forgot_pass.setOnClickListener { startActivity(Intent(this,ForgotPassActivity::class.java)) }
        login.setOnClickListener { if (CommonUtil.checkEditError(phone,getString(R.string.enter_phone))||
                CommonUtil.checkEditError(password,getString(R.string.enter_password))){
        return@setOnClickListener
        }else{
            SignIn()
        }
        }
        view.setOnClickListener { if (password?.inputType== InputType.TYPE_TEXT_VARIATION_NORMAL or InputType.TYPE_CLASS_TEXT){
            password?.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

        }else{
            password?.inputType = InputType.TYPE_TEXT_VARIATION_NORMAL or InputType.TYPE_CLASS_TEXT
        }
        }
        visitor.setOnClickListener {
            mSharedPrefManager.loginStatus = false
            startActivity(Intent(this,MainActivity::class.java))
        }


    }

    fun SignIn(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Login(mLanguagePrefManager.appLanguage,phone.text.toString(),password.text.toString()
        ,deviceID,"android")?.enqueue(object : Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        mSharedPrefManager.loginStatus = true
                        mSharedPrefManager.userData = response.body()?.data!!
                        startActivity(Intent(this@LoginActivity,MainActivity::class.java))
                        finish()
                    }else if (response.body()?.value.equals("2")){
                        val intent = Intent(this@LoginActivity,ActivateAccountActivity::class.java)
                        intent.putExtra("user",response.body()?.data)
                        startActivity(intent)
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}