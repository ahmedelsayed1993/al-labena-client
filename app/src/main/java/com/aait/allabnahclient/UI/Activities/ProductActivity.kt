package com.aait.allabnahclient.UI.Activities

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.util.Log
import android.view.View
import android.widget.*
import androidx.annotation.Nullable
import androidx.core.app.ActivityCompat
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.allabnahclient.Base.Parent_Activity
import com.aait.allabnahclient.Cart.AddCartViewModel
import com.aait.allabnahclient.Cart.AllCartViewModel
import com.aait.allabnahclient.Cart.CartDataBase
import com.aait.allabnahclient.Cart.CartModelOffline
import com.aait.allabnahclient.Client
import com.aait.allabnahclient.Listeners.OnItemClickListener
import com.aait.allabnahclient.Models.*
import com.aait.allabnahclient.Network.Service
import com.aait.allabnahclient.R
import com.aait.allabnahclient.UI.Adapters.ColorsAdapter
import com.aait.allabnahclient.UI.Adapters.SliderAdapter
import com.aait.allabnahclient.Uitls.CommonUtil
import com.aait.allabnahclient.Uitls.PermissionUtils
import com.github.islamkhsh.CardSliderViewPager
import com.google.gson.Gson
import me.relex.circleindicator.CircleIndicator
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProductActivity:Parent_Activity(),OnItemClickListener {
    override fun onItemClick(view: View, position: Int) {
        color = colorModels.get(position).color!!
        color_id = colorModels.get(position).id!!.toString()

    }

    override val layoutResource: Int
        get() = R.layout.activity_product_details

    lateinit var fav:ImageView
    lateinit var viewPager:CardSliderViewPager
    lateinit var indicator:CircleIndicator
    lateinit var name:TextView
    lateinit var rate:TextView
    lateinit var description:TextView
    lateinit var linear_meter:TextView
    lateinit var add:ImageView
    lateinit var count:TextView
    lateinit var minus:ImageView
    lateinit var colors:RecyclerView
    lateinit var total:TextView
    lateinit var check:CheckBox
    lateinit var add_basket:Button
    lateinit var whats:Button
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var colorsAdapter: ColorsAdapter
    var colorModels = ArrayList<ColorModel>()
    lateinit var price:TextView
    var id = 0
    var i = 1
    var minimum = 0
    var color = ""
    var color_id = ""
    var total_price = 0
    lateinit var number:TextView
    lateinit var num_cars:TextView
    lateinit var lay:LinearLayout
    lateinit var total_prices:TextView
    lateinit var con:Button
    lateinit var done:Button
    lateinit var subCategoryDetailsModel: SubCategoryDetailsModel
    override lateinit var addCartViewModel: AddCartViewModel
    private var allCartViewModel: AllCartViewModel? = null
    internal lateinit var cartDataBase: CartDataBase


    internal lateinit var cartModels: LiveData<List<CartModelOffline>>

    internal var cartModelOfflines: List<CartModelOffline> = java.util.ArrayList()
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        cartDataBase = CartDataBase.getDataBase(mContext)
        addCartViewModel = ViewModelProviders.of(this).get(AddCartViewModel::class.java)
        allCartViewModel = ViewModelProviders.of(this).get(AllCartViewModel::class.java)
        addCartViewModel = AddCartViewModel(application)
        fav = findViewById(R.id.fav)
        viewPager = findViewById(R.id.viewPager)
        indicator = findViewById(R.id.indicator)
        name = findViewById(R.id.name)
        rate = findViewById(R.id.rate)
        description = findViewById(R.id.description)
        linear_meter = findViewById(R.id.linear_meter)
        lay = findViewById(R.id.lay)
        total_prices = findViewById(R.id.total_price)
        con = findViewById(R.id.con)
        done = findViewById(R.id.done)
        add = findViewById(R.id.add)
        count = findViewById(R.id.count)
        minus = findViewById(R.id.minus)
        colors = findViewById(R.id.colors)
        total = findViewById(R.id.total)
        check = findViewById(R.id.check)
        add_basket = findViewById(R.id.add_basket)
        whats = findViewById(R.id.whats)
        price = findViewById(R.id.price)
        number = findViewById(R.id.number)
        num_cars = findViewById(R.id.num_cars)
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        colorsAdapter = ColorsAdapter(mContext,colorModels,R.layout.recycler_color)
        colorsAdapter.setOnItemClickListener(this)
        colors.layoutManager = linearLayoutManager
        colors.adapter = colorsAdapter
        num_cars.text = i.toString()
        lay.visibility = View.GONE
        if(mSharedPrefManager.loginStatus!!){
        getData(mSharedPrefManager.userData.id)
        }else{
            getData(null)
        }
        fav.setOnClickListener {
            if(mSharedPrefManager.loginStatus!!){
                AddToFav(id)
            }else{
                CommonUtil.makeToast(mContext,getString(R.string.login_first))
            }
        }
        add.setOnClickListener {
            i = i+1
            count.text =(minimum*i).toString()
            total.text = (total_price *minimum* i).toString()
            num_cars.text = i.toString()
        }
        minus.setOnClickListener {
            if(i>1){
                i = i-1
                count.text =(minimum*i).toString()
                total.text = (total_price *minimum* i).toString()
                num_cars.text = i.toString()
            }
        }

        con.setOnClickListener {  onBackPressed()
        finish()
        }
        done.setOnClickListener { startActivity(Intent(this,MainActivity::class.java)) }
        whats.setOnClickListener { getLocationWithPermission(subCategoryDetailsModel.whatsapp!!) }
        add_basket.setOnClickListener {
            if(subCategoryDetailsModel.brick_number.equals("")){
                if(color.equals("")){
                    CommonUtil.makeToast(mContext,getString(R.string.choose_color))
                }else{
                    addCartViewModel.addItem(CartModelOffline(id,subCategoryDetailsModel.name,subCategoryDetailsModel.images?.get(0),
                        count.text.toString(),color,color_id,subCategoryDetailsModel.brick_number,num_cars.text.toString(),total.text.toString()))
                    //Log.e("cart", Gson().toJson(addCartViewModel.cart))
                    allCartViewModel!!.allCart.observe(this, object : Observer<List<CartModelOffline>> {
                        override fun onChanged(@Nullable cartModels: List<CartModelOffline>) {
                            cartModelOfflines = cartModels
                            Log.e("carts", Gson().toJson(cartModels))
                            if(cartModels.isEmpty()){
                                lay.visibility = View.GONE
                            }else{
                                lay.visibility = View.VISIBLE
                                var price = 0
                                for (i in 0..cartModels.size-1){
                                    price = price+cartModels.get(i).price!!.toInt()
                                }
                                total_prices.text = price.toString() + getString(R.string.rs)


                            }



                        }
                    })
                }
            }else {
                addCartViewModel.addItem(
                    CartModelOffline(
                        id,
                        subCategoryDetailsModel.name,
                        subCategoryDetailsModel.images?.get(0),
                        count.text.toString(),
                        color,
                        color_id,
                        subCategoryDetailsModel.brick_number,
                        num_cars.text.toString(),
                        total.text.toString()
                    )
                )
                //Log.e("cart", Gson().toJson(addCartViewModel.cart))
                allCartViewModel!!.allCart.observe(this, object : Observer<List<CartModelOffline>> {
                    override fun onChanged(@Nullable cartModels: List<CartModelOffline>) {
                        cartModelOfflines = cartModels
                        Log.e("carts", Gson().toJson(cartModels))
                        if (cartModels.isEmpty()) {
                            lay.visibility = View.GONE
                        } else {
                            lay.visibility = View.VISIBLE


                        }


                    }
                })
            }
        }

    }

    internal fun callnumber(number: String) {
        val intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse("tel:$number")
        mContext!!.startActivity(intent)
    }

    fun getLocationWithPermission(number: String) {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!PermissionUtils.hasPermissions(mContext, Manifest.permission.CALL_PHONE)) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                    ActivityCompat.requestPermissions(
                        mContext as Activity, PermissionUtils.CALL_PHONE,
                        300
                    )
            } else {
                callnumber(number)
            }
        } else {
            callnumber(number)
        }

    }


    fun getData(user_id:Int?){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Details(mLanguagePrefManager.appLanguage,id,user_id)
            ?.enqueue(object : Callback<SubCategoryDetailsResponse> {
                override fun onFailure(call: Call<SubCategoryDetailsResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<SubCategoryDetailsResponse>,
                    response: Response<SubCategoryDetailsResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            subCategoryDetailsModel = response.body()?.data!!
                            initSliderAds(response.body()?.data?.images!!)
                            name.text = response.body()?.data?.name
                            description.text = response.body()?.data?.description
                            rate.text = response.body()?.data?.rates
                            linear_meter.text = response.body()?.data?.price+getString(R.string.rs)
                            total_price = response.body()?.data?.price!!.toInt()
                            count.text = response.body()?.data?.minimum
                            number.text = getString(R.string.number)+response.body()?.data?.weight
                            minimum =  response.body()?.data?.minimum!!.toInt()
                            price.text = getString(R.string.price)+response.body()?.data?.weight
                            colorsAdapter.updateAll(response.body()?.data?.colors!!)
                            if (response.body()?.data?.is_favorite==1){
                                fav.setImageResource(R.mipmap.heart)
                            }else{
                                fav.setImageResource(R.mipmap.nounheart)
                            }
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }
            })
    }
    fun initSliderAds(list:ArrayList<String>){
        if(list.isEmpty()){
            viewPager.visibility= View.GONE
            indicator.visibility= View.GONE
        }
        else{
            viewPager.visibility= View.VISIBLE
            indicator.visibility= View.VISIBLE
            viewPager.adapter= SliderAdapter(mContext,list)
            indicator.setViewPager(viewPager)
        }
    }

    fun AddToFav(id:Int){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.AddFav(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,id)
            ?.enqueue(object :Callback<FavResponse>{
                override fun onFailure(call: Call<FavResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<FavResponse>,
                    response: Response<FavResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            if (response.body()?.data==1){
                                fav.setImageResource(R.mipmap.heart)
                            }else{
                                fav.setImageResource(R.mipmap.nounheart)
                            }
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

            })
    }
}