package com.aait.allabnahclient.UI.Fragments

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.allabnahclient.Base.BaseFragment
import com.aait.allabnahclient.Client
import com.aait.allabnahclient.Listeners.OnItemClickListener
import com.aait.allabnahclient.Models.NotificationModel
import com.aait.allabnahclient.Models.NotificationResponse
import com.aait.allabnahclient.Models.TermsResponse
import com.aait.allabnahclient.Network.Service
import com.aait.allabnahclient.R
import com.aait.allabnahclient.UI.Activities.OrdersActivity
import com.aait.allabnahclient.UI.Adapters.NotificationAdapter
import com.aait.allabnahclient.Uitls.CommonUtil
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NotificationFragment:BaseFragment(),OnItemClickListener {
    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.delete) {
            showProgressDialog(getString(R.string.please_wait))
            Client.getClient()?.create(Service::class.java)?.delete(
                mSharedPrefManager.userData.id!!
                , notificationModels.get(position).id!!
            ,mLanguagePrefManager.appLanguage)?.enqueue(object :
                Callback<TermsResponse> {
                override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext!!, t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<TermsResponse>,
                    response: Response<TermsResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful) {
                        if (response.body()?.value.equals("1")) {
                            CommonUtil.makeToast(mContext!!, response.body()?.data!!)
                            getData()
                        } else {
                            CommonUtil.makeToast(mContext!!, response.body()?.msg!!)
                        }
                    }
                }

            })
        }else if (view.id == R.id.lay){
//            val intent = Intent(activity, OrderDetailsActivity::class.java)
//            intent.putExtra("id",notificationModels.get(position).order_id)
//            startActivity(intent)
        }
    }

    override val layoutResource: Int
        get() = R.layout.fragment_notification
    companion object {
        fun newInstance(): NotificationFragment {
            val args = Bundle()
            val fragment = NotificationFragment()
            fragment.setArguments(args)
            return fragment
        }
    }
    lateinit var title: TextView
    lateinit var back: ImageView
    lateinit var notify: ImageView
    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null

    internal var layNoItem: RelativeLayout? = null

    internal var tvNoContent: TextView? = null

    var swipeRefresh: SwipeRefreshLayout? = null
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var notificationAdapter: NotificationAdapter
    var notificationModels = ArrayList<NotificationModel>()
    override fun initializeComponents(view: View) {
        title = view.findViewById(R.id.title)
        back = view.findViewById(R.id.back)

        notify = view.findViewById(R.id.notify)
        notify.visibility = View.GONE
        back.visibility = View.GONE
        title.text = getString(R.string.notification)
        rv_recycle = view.findViewById(R.id.rv_recycle)
        layNoInternet = view.findViewById(R.id.lay_no_internet)
        layNoItem = view.findViewById(R.id.lay_no_item)
        tvNoContent = view.findViewById(R.id.tv_no_content)
        swipeRefresh = view.findViewById(R.id.swipe_refresh)
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        notificationAdapter = NotificationAdapter(mContext!!,notificationModels,R.layout.recycler_notifications)
        notificationAdapter.setOnItemClickListener(this)
        rv_recycle.layoutManager = linearLayoutManager
        rv_recycle.adapter = notificationAdapter

        swipeRefresh!!.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener {
            if(mSharedPrefManager.loginStatus!!){
                getData()
            }else{
                CommonUtil.makeToast(mContext!!,getString(R.string.login_first))
            }


        }
        if(mSharedPrefManager.loginStatus!!){
            getData()
        }else{
            CommonUtil.makeToast(mContext!!,getString(R.string.login_first))
        }

    }

    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.Notifications(mSharedPrefManager.userData.id!!,mLanguagePrefManager.appLanguage)?.enqueue(object :
            Callback<NotificationResponse> {
            override fun onResponse(call: Call<NotificationResponse>, response: Response<NotificationResponse>) {
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
                if (response.isSuccessful) {
                    try {
                        if (response.body()?.value.equals("1")) {
                            Log.e("myJobs", Gson().toJson(response.body()!!.data))
                            if (response.body()!!.data?.isEmpty()!!) {
                                layNoItem!!.visibility = View.VISIBLE
                                layNoInternet!!.visibility = View.GONE
                                tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)
                            } else {
//                            initSliderAds(response.body()?.slider!!)
                                notificationAdapter.updateAll(response.body()!!.data!!)
                            }
//                            response.body()!!.data?.let { homeSeekerAdapter.updateAll(it)
                        }else {
                            CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                        }

                    } catch (e: Exception) {e.printStackTrace() }

                } else {  }
            }
            override fun onFailure(call: Call<NotificationResponse>, t: Throwable) {
                CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
                Log.e("response", Gson().toJson(t))
            }
        })


    }
}