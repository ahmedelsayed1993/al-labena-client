package com.aait.allabnahclient.UI.Activities

import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.*
import androidx.annotation.Nullable
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.aait.allabnahclient.Base.Parent_Activity
import com.aait.allabnahclient.Cart.AllCartViewModel
import com.aait.allabnahclient.Cart.CartDataBase
import com.aait.allabnahclient.Cart.CartModelOffline
import com.aait.allabnahclient.Client
import com.aait.allabnahclient.Listeners.OnItemClickListener
import com.aait.allabnahclient.Models.BrickModel
import com.aait.allabnahclient.Models.FavResponse
import com.aait.allabnahclient.Models.ListModel
import com.aait.allabnahclient.Models.ListResponse
import com.aait.allabnahclient.Network.Service
import com.aait.allabnahclient.R
import com.aait.allabnahclient.UI.Views.ListDialog
import com.aait.allabnahclient.Uitls.CommonUtil
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CompleteDeliveryActivity:Parent_Activity(),OnItemClickListener{
    override fun onItemClick(view: View, position: Int) {
        listDialog.dismiss()
        if (selected == 0){
            cityModel = citiesModels.get(position)
            city.text = cityModel.name
            city_id = cityModel.id!!

        }else if (selected == 1){
            regionModel = regionsModels.get(position)
            district.text = regionModel.name
        }


    }

    override val layoutResource: Int
        get() = R.layout.activity_delivery_info
    lateinit var city:TextView
    lateinit var district:TextView
    lateinit var street:EditText
    lateinit var name:EditText
    lateinit var phone:EditText
    lateinit var location:TextView
    lateinit var previous_address:TextView
    lateinit var complete:Button
    var lat = ""
    var lng = ""
    var address = ""
    var brickModels = ArrayList<BrickModel>()
    var num = 0
    var city_id = 0
    lateinit var listDialog: ListDialog
     var citiesModels = ArrayList<ListModel>()
     var regionsModels = ArrayList<ListModel>()
    lateinit var cityModel: ListModel
    lateinit var regionModel: ListModel
    var tax = 0
    var delivery_price = 0
    var total = 0
    var final_total = 0
    var selected = 0
    lateinit var title:TextView
    lateinit var back:ImageView
    lateinit var cartOffline: CartModelOffline
    private var allCartViewModel: AllCartViewModel? = null
    internal lateinit var cartModels: LiveData<List<CartModelOffline>>
    internal lateinit var cartDataBase: CartDataBase
    internal var cartModelOfflines: List<CartModelOffline> = java.util.ArrayList()
    override fun initializeComponents() {
        brickModels = intent.getSerializableExtra("order") as ArrayList<BrickModel>
        num = intent.getIntExtra("num",0)
        tax = intent.getIntExtra("tax",0)
        delivery_price = intent.getIntExtra("delivery_price",0)
        total = intent.getIntExtra("total",0)
        final_total = intent.getIntExtra("final_total",0)
        Log.e("num",num.toString())
        Log.e("order", Gson().toJson(brickModels))
        title = findViewById(R.id.title)
        back = findViewById(R.id.back)
        title.text = getString(R.string.Delivery_information)
        back.setOnClickListener { onBackPressed()
        finish()}
        cartDataBase = CartDataBase.getDataBase(mContext!!)
        allCartViewModel = ViewModelProviders.of(this).get(AllCartViewModel::class.java)
        cartModels = allCartViewModel!!.allCart
        allCartViewModel = ViewModelProviders.of(this).get(AllCartViewModel::class.java)
        allCartViewModel!!.allCart.observe(this, object : Observer<List<CartModelOffline>> {
            override fun onChanged(@Nullable cartModels: List<CartModelOffline>) {
                cartModelOfflines = cartModels
            }
        })
        city = findViewById(R.id.city)
        district = findViewById(R.id.district)
        street = findViewById(R.id.street)
        name = findViewById(R.id.name)
        phone = findViewById(R.id.phone)
        location = findViewById(R.id.location)
        previous_address = findViewById(R.id.previous_address)
        complete = findViewById(R.id.complete)
        city.setOnClickListener {
            selected = 0
            getCities()
        }
        district.setOnClickListener {
            selected = 1
            if (city_id == 0){
                CommonUtil.makeToast(mContext,getString(R.string.choose_city))
            }else{
                getRegions(city_id)
            }
        }
        location.setOnClickListener { startActivityForResult(Intent(this@CompleteDeliveryActivity,LocationActivity::class.java),1) }
        complete.setOnClickListener {
            if (CommonUtil.checkTextError(city,getString(R.string.enter_city))||
                CommonUtil.checkTextError(district,getString(R.string.enter_district))||
                        CommonUtil.checkEditError(street,getString(R.string.enter_street))||
                CommonUtil.checkEditError(name,getString(R.string.enter_name))||
                CommonUtil.checkEditError(phone,getString(R.string.enter_phone))||
                CommonUtil.checkLength1(phone,getString(R.string.phone_length),10)||
                CommonUtil.checkTextError(location,getString(R.string.choose_location))
            ){
                return@setOnClickListener
            }else{
                if(mSharedPrefManager.loginStatus!!){
                    Add()
                }else{
                    startActivity(Intent(this,LoginActivity::class.java))
                    finish()
                }

            }
        }

    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (data?.getStringExtra("result") != null) {
            address = data?.getStringExtra("result").toString()
            lat = data?.getStringExtra("lat").toString()
            lng = data?.getStringExtra("lng").toString()
            location.text = address
        } else {
            address = ""
            lat = ""
            lng = ""
            location.text = ""
        }

    }

    fun getCities(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Cities(mLanguagePrefManager.appLanguage)?.enqueue(object:
            Callback<ListResponse>{
            override fun onFailure(call: Call<ListResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<ListResponse>, response: Response<ListResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        citiesModels = response.body()?.data!!
                        listDialog = ListDialog(mContext,this@CompleteDeliveryActivity,citiesModels,getString(R.string.city))
                        listDialog.show()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    fun getRegions(id:Int){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Regions(mLanguagePrefManager.appLanguage,id)?.enqueue(object:
            Callback<ListResponse>{
            override fun onFailure(call: Call<ListResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<ListResponse>, response: Response<ListResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        regionsModels = response.body()?.data!!
                        listDialog = ListDialog(mContext,this@CompleteDeliveryActivity,regionsModels,getString(R.string.District))
                        listDialog.show()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    fun Add(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.AddOrder(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,Gson().toJson(brickModels)
        ,cityModel.id!!,regionModel.id!!,brickModels.get(0).color_id!!.toInt(),street.text.toString(),name.text.toString()
        ,phone.text.toString(),lat,lng,location.text.toString(),delivery_price.toString(),tax.toString(),total.toString()
        ,num,final_total.toString())?.enqueue(object : Callback<FavResponse>{
            override fun onFailure(call: Call<FavResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<FavResponse>, response: Response<FavResponse>) {
                hideProgressDialog()
                if (response.isSuccessful) {
                    if (response.body()?.value.equals("1")) {
                        val intent = Intent(this@CompleteDeliveryActivity,PayActivity::class.java)
                            intent.putExtra("id",response.body()?.data)
                            intent.putExtra("amount",final_total)
                            startActivity(intent)
                            finish()
                        allCartViewModel!!.deleteItems(cartModelOfflines)
                        CartDataBase.closeDataBase()

                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}