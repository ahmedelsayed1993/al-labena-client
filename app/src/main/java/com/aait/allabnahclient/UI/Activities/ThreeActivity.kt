package com.aait.allabnahclient.UI.Activities

import android.content.Intent
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.aait.allabnahclient.Base.Parent_Activity
import com.aait.allabnahclient.Client
import com.aait.allabnahclient.Models.ScreenResponse
import com.aait.allabnahclient.Network.Service
import com.aait.allabnahclient.R
import com.aait.allabnahclient.Uitls.CommonUtil
import com.bumptech.glide.Glide
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ThreeActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_three
lateinit var next:Button
    lateinit var title: TextView
    lateinit var content: TextView
    lateinit var image: ImageView
    override fun initializeComponents() {
        next = findViewById(R.id.next)
        title = findViewById(R.id.title)
        content = findViewById(R.id.content)
        image = findViewById(R.id.image)
        next.setOnClickListener { startActivity(Intent(this,ChooseLangActivity::class.java))
        finish()}
        getData()
    }
    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Sreens(mLanguagePrefManager.appLanguage)?.enqueue(object:
            Callback<ScreenResponse> {
            override fun onFailure(call: Call<ScreenResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<ScreenResponse>, response: Response<ScreenResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        Glide.with(mContext).asBitmap().load(response.body()?.data?.get(2)?.image).into(image)
                        title.text = response.body()?.data?.get(2)?.title
                        content.text = response.body()?.data?.get(2)?.content
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}