package com.aait.allabnahclient.UI.Activities

import android.content.Intent
import android.widget.ImageView
import android.widget.RadioButton
import android.widget.TextView
import com.aait.allabnahclient.Base.Parent_Activity
import com.aait.allabnahclient.R

class PayActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_pay_methods
    lateinit var title:TextView
    lateinit var back:ImageView
    lateinit var online:RadioButton
    lateinit var bank:RadioButton
    var id = 0
    var amount = 0

    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        amount = intent.getIntExtra("amount",0)
        title = findViewById(R.id.title)
        back = findViewById(R.id.back)
        online = findViewById(R.id.online)
        bank = findViewById(R.id.bank)
        title.text = getString(R.string.Online_payment)
        back.setOnClickListener { startActivity(Intent(this,MainActivity::class.java))
        finish()}
        bank.setOnClickListener { val intent = Intent(this,BankActivity::class.java)
            intent.putExtra("id",id)
            intent.putExtra("amount",amount)
            startActivity(intent)
            finish()
        }

    }
}