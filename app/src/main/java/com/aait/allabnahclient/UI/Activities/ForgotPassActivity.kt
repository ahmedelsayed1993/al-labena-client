package com.aait.allabnahclient.UI.Activities

import android.content.Intent
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import com.aait.allabnahclient.Base.Parent_Activity
import com.aait.allabnahclient.Client
import com.aait.allabnahclient.Models.UserResponse
import com.aait.allabnahclient.Network.Service
import com.aait.allabnahclient.R
import com.aait.allabnahclient.Uitls.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ForgotPassActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_forgot_pass
    lateinit var back:ImageView
    lateinit var phone:EditText
    lateinit var next:Button

    override fun initializeComponents() {
        back = findViewById(R.id.back)
        phone = findViewById(R.id.phone)
        next = findViewById(R.id.next)
        back.setOnClickListener { startActivity(Intent(this,LoginActivity::class.java)) }
        next.setOnClickListener { if (CommonUtil.checkEditError(phone,getString(R.string.enter_phone))){
        return@setOnClickListener
        }else{

            ForgotPass()
        }
        }

    }
    fun ForgotPass(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.ForGot(phone.text.toString(),mLanguagePrefManager.appLanguage)?.enqueue(object :
            Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        if (response.body()?.data?.user_type.equals("user")) {
                            val intent =
                                Intent(this@ForgotPassActivity, NewPasswordActivity::class.java)
                            intent.putExtra("data", response.body()?.data)
                            startActivity(intent)
                            finish()
                        }else{
                            CommonUtil.makeToast(mContext,"هذا ليس حساب مستخدم")
                        }
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })

    }
}