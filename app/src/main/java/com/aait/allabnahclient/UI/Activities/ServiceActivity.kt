package com.aait.allabnahclient.UI.Activities

import android.content.Intent
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.aait.allabnahclient.Base.Parent_Activity
import com.aait.allabnahclient.Models.CategoryModel
import com.aait.allabnahclient.R

class ServiceActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_service
    lateinit var title:TextView
    lateinit var back:ImageView
    lateinit var services: RelativeLayout
    lateinit var consulting: RelativeLayout
    lateinit var complains: RelativeLayout
    lateinit var categoryModel: CategoryModel
    override fun initializeComponents() {
        categoryModel = intent.getSerializableExtra("category") as CategoryModel
        title = findViewById(R.id.title)
        back = findViewById(R.id.back)
        services = findViewById(R.id.services)
        consulting = findViewById(R.id.consulting)
        complains = findViewById(R.id.complains)
        back.setOnClickListener { onBackPressed()
            finish()}
        title.text = categoryModel.name
        services.setOnClickListener {
            startActivity(Intent(this,ServicesActivity::class.java))
        }
        consulting.setOnClickListener {
            val intent = Intent(this,RequestServicesActivity::class.java)
            intent.putExtra("name",getString(R.string.Consulting))
            intent.putExtra("type","consulting")
            startActivity(intent)
        }
        complains.setOnClickListener {
            val intent = Intent(this,RequestServicesActivity::class.java)
            intent.putExtra("name",getString(R.string.Complaints))
            intent.putExtra("type","complains")
            startActivity(intent)
        }

    }
}