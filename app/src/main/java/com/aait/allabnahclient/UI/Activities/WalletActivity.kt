package com.aait.allabnahclient.UI.Activities

import android.content.Context
import android.widget.ImageView
import android.widget.TextView
import com.aait.allabnahclient.Base.Parent_Activity
import com.aait.allabnahclient.Client
import com.aait.allabnahclient.Models.TermsResponse
import com.aait.allabnahclient.Network.Service
import com.aait.allabnahclient.R
import com.aait.allabnahclient.Uitls.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class WalletActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_wallet

    lateinit var title:TextView
    lateinit var back:ImageView
    lateinit var balance:TextView
    override fun initializeComponents() {
        title = findViewById(R.id.title)
        back = findViewById(R.id.back)
        balance = findViewById(R.id.balance)
        title.text = getString(R.string.wallet)
        back.setOnClickListener { onBackPressed() }
        getData()

    }

    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Balance(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!)
            ?.enqueue(object : Callback<TermsResponse> {
                override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<TermsResponse>,
                    response: Response<TermsResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            balance.text = response.body()?.data + getString(R.string.rs)
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

            })
    }
}