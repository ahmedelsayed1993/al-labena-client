package com.aait.allabnahclient.UI.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.aait.allabnahclient.Base.ParentRecyclerAdapter
import com.aait.allabnahclient.Base.ParentRecyclerViewHolder
import com.aait.allabnahclient.Models.CategoryModel
import com.aait.allabnahclient.Models.ColorModel
import com.aait.allabnahclient.R
import com.bumptech.glide.Glide

class ColorsAdapter (context: Context, data: MutableList<ColorModel>, layoutId: Int) :
    ParentRecyclerAdapter<ColorModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val questionModel = data.get(position)
        viewHolder.color!!.setText(questionModel.color)



        viewHolder.itemView.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)

        })



    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {





        internal var color=itemView.findViewById<TextView>(R.id.color)





    }
}