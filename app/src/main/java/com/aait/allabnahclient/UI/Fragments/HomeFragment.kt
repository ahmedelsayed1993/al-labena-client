package com.aait.allabnahclient.UI.Fragments

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.allabnahclient.Base.BaseFragment
import com.aait.allabnahclient.Client
import com.aait.allabnahclient.Listeners.OnItemClickListener
import com.aait.allabnahclient.Models.CategoryModel
import com.aait.allabnahclient.Models.CategoryResponse

import com.aait.allabnahclient.Models.QuestionModel
import com.aait.allabnahclient.Network.Service
import com.aait.allabnahclient.R
import com.aait.allabnahclient.UI.Activities.RequestServiceActivity
import com.aait.allabnahclient.UI.Activities.ServiceActivity
import com.aait.allabnahclient.UI.Activities.SubCategoryActivity
import com.aait.allabnahclient.UI.Adapters.CategoryAdapter

import com.aait.allabnahclient.UI.Adapters.QuestionsAdapter
import com.aait.allabnahclient.UI.Adapters.SliderAdapter
import com.aait.allabnahclient.Uitls.CommonUtil
import com.github.islamkhsh.CardSliderViewPager
import com.google.gson.Gson
import me.relex.circleindicator.CircleIndicator
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomeFragment:BaseFragment(),OnItemClickListener {
    override fun onItemClick(view: View, position: Int) {
       if (categoriesModels.get(position).category_type.equals("service")) {
           val intent = Intent(activity, SubCategoryActivity::class.java)
           intent.putExtra("category", categoriesModels.get(position))
           startActivity(intent)
       }else if(categoriesModels.get(position).category_type.equals("consulting")){
           val intent = Intent(activity, ServiceActivity::class.java)
           intent.putExtra("category", categoriesModels.get(position))
           startActivity(intent)
       }else{
           val intent = Intent(activity, RequestServiceActivity::class.java)
           intent.putExtra("category", categoriesModels.get(position))
           startActivity(intent)
       }
    }

    override val layoutResource: Int
        get() = R.layout.fragment_home
    companion object {
        fun newInstance(): HomeFragment {
            val args = Bundle()
            val fragment = HomeFragment()
            fragment.setArguments(args)
            return fragment
        }
    }
    lateinit var title:TextView
    lateinit var back:ImageView
    lateinit var notify:ImageView
    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null

    internal var layNoItem: RelativeLayout? = null

    internal var tvNoContent: TextView? = null

    var swipeRefresh: SwipeRefreshLayout? = null
    internal lateinit var gridLayoutManager: GridLayoutManager
    internal var categoriesModels = java.util.ArrayList<CategoryModel>()
    internal lateinit var categoryAdapter: CategoryAdapter
    lateinit var viewpager: CardSliderViewPager
    lateinit var indicator: CircleIndicator
    override fun initializeComponents(view: View) {
        viewpager = view.findViewById(R.id.viewPager)
        indicator = view.findViewById(R.id.indicator)
        rv_recycle = view.findViewById(R.id.rv_recycle)
        layNoInternet = view.findViewById(R.id.lay_no_internet)
        layNoItem = view.findViewById(R.id.lay_no_item)
        tvNoContent = view.findViewById(R.id.tv_no_content)
        swipeRefresh = view.findViewById(R.id.swipe_refresh)
        title = view.findViewById(R.id.title)
        back = view.findViewById(R.id.back)
        notify = view.findViewById(R.id.notify)
        title.text = getString(R.string.home)
        notify.visibility = View.GONE
        back.visibility = View.GONE
        gridLayoutManager = GridLayoutManager(mContext,2)
        categoryAdapter = CategoryAdapter(mContext!!,categoriesModels,R.layout.recycler_category)
        categoryAdapter.setOnItemClickListener(this)
        rv_recycle.layoutManager = gridLayoutManager
        rv_recycle.adapter = categoryAdapter



        swipeRefresh!!.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener {
            getData()

        }
        getData()

    }
    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.Categories(mLanguagePrefManager.appLanguage)?.enqueue(object :
            Callback<CategoryResponse>{
            override fun onResponse(call: Call<CategoryResponse>, response: Response<CategoryResponse>) {
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
                if (response.isSuccessful) {

                    if (response.body()?.value.equals("1")) {
                        initSliderAds(response.body()?.sliders!!)
                        if (response.body()!!.data?.isEmpty()!!) {
                            layNoItem!!.visibility = View.VISIBLE
                            layNoInternet!!.visibility = View.GONE
                            tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)
                        } else {


                            categoryAdapter.updateAll(response.body()!!.data!!)
                        }

                    } else {}

                } else {  }
            }
            override fun onFailure(call: Call<CategoryResponse>, t: Throwable) {
                 CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                Log.e("response", Gson().toJson(t))
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
            }
        })

    }

    fun initSliderAds(list:ArrayList<String>){
        if(list.isEmpty()){
            viewpager.visibility=View.GONE
            indicator.visibility=View.GONE
        }
        else{
            viewpager.visibility=View.VISIBLE
            indicator.visibility=View.VISIBLE
            viewpager.adapter= SliderAdapter(context!!,list)
            indicator.setViewPager(viewpager)
        }
    }
}