package com.aait.allabnahclient.UI.Activities

import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.aait.allabnahclient.Base.Parent_Activity
import com.aait.allabnahclient.Client
import com.aait.allabnahclient.Models.OrderDetailsResponse
import com.aait.allabnahclient.Network.Service
import com.aait.allabnahclient.R
import com.aait.allabnahclient.UI.Adapters.SliderAdapter
import com.aait.allabnahclient.Uitls.CommonUtil
import com.github.islamkhsh.CardSliderViewPager
import me.relex.circleindicator.CircleIndicator
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ActivityOrderDetails:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_order_details
       lateinit var back:ImageView
    lateinit var viewPager: CardSliderViewPager
    lateinit var indicator: CircleIndicator
    lateinit var name: TextView
    lateinit var description:TextView
    lateinit var code:TextView
    lateinit var count_of_total:TextView
    lateinit var num:LinearLayout
    lateinit var black_num:TextView
    lateinit var color:LinearLayout
    lateinit var the_color:TextView
    lateinit var price_before_vit:TextView
    lateinit var delivery_before_tax:TextView

    lateinit var transportation_cast:TextView
    lateinit var total_before_tax:TextView
    lateinit var added_tax:TextView
    lateinit var total_after_tax:TextView
    lateinit var tax_value:TextView
    lateinit var complete:Button
    var id = 0
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        back = findViewById(R.id.back)
        viewPager = findViewById(R.id.viewPager)
        indicator = findViewById(R.id.indicator)
        name = findViewById(R.id.name)
        description = findViewById(R.id.description)
        code = findViewById(R.id.code)
        count_of_total = findViewById(R.id.count_of_total)
        num = findViewById(R.id.num)
        black_num = findViewById(R.id.black_num)
        color = findViewById(R.id.color)
        the_color = findViewById(R.id.the_color)
        price_before_vit = findViewById(R.id.price_before_vit)
        delivery_before_tax = findViewById(R.id.delivery_before_tax)

        transportation_cast = findViewById(R.id.transportation_cast)
        added_tax = findViewById(R.id.added_tax)
        total_before_tax = findViewById(R.id.total_before_tax)
        total_after_tax = findViewById(R.id.total_after_tax)
        tax_value = findViewById(R.id.tax_value)
        complete = findViewById(R.id.complete)
        complete.visibility = View.GONE
        getData()


    }

    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Detailsorder(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,id,"user")
            ?.enqueue(object : Callback<OrderDetailsResponse> {
                override fun onFailure(call: Call<OrderDetailsResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<OrderDetailsResponse>,
                    response: Response<OrderDetailsResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            initSliderAds(response.body()?.data?.image!!)
                            if (response.body()?.data?.brick_number.equals("")){
                                num.visibility = View.GONE
                                color.visibility = View.VISIBLE
                                the_color.text = response.body()?.data?.color
                            }else{
                                num.visibility = View.VISIBLE
                                color.visibility = View.GONE
                                black_num.text = response.body()?.data?.brick_number
                            }
                            name.text = response.body()?.data?.name
                            description.text = response.body()?.data?.description
                            code.text = getString(R.string.code) + response.body()?.data?.code + "#"
                            count_of_total.text = response.body()?.data?.count + getString(R.string.rs)
                            price_before_vit.text =response.body()?.data?.total + getString(R.string.rs)
                            delivery_before_tax.text =response.body()?.data?.delivery + getString(R.string.rs)
                            tax_value.text = getString(R.string.added_tax)+response.body()?.data?.tax + "%"
                            added_tax.text = response.body()?.data?.total_tax + getString(R.string.rs)
                            total_before_tax.text = response.body()?.data?.total + getString(R.string.rs)
                            total_after_tax.text = response.body()?.data?.total_amount + getString(R.string.rs)
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }
            })
    }

    fun initSliderAds(list:ArrayList<String>){
        if(list.isEmpty()){
            viewPager.visibility= View.GONE
            indicator.visibility= View.GONE
        }
        else{
            viewPager.visibility= View.VISIBLE
            indicator.visibility= View.VISIBLE
            viewPager.adapter= SliderAdapter(mContext,list)
            indicator.setViewPager(viewPager)
        }
    }
}