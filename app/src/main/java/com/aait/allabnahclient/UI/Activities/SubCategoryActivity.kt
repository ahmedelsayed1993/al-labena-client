package com.aait.allabnahclient.UI.Activities

import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.allabnahclient.Base.Parent_Activity
import com.aait.allabnahclient.Client
import com.aait.allabnahclient.Listeners.OnItemClickListener
import com.aait.allabnahclient.Models.*
import com.aait.allabnahclient.Network.Service
import com.aait.allabnahclient.R
import com.aait.allabnahclient.UI.Adapters.QuestionsAdapter
import com.aait.allabnahclient.UI.Adapters.SubCategoryAdapter
import com.aait.allabnahclient.Uitls.CommonUtil
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SubCategoryActivity :Parent_Activity(),OnItemClickListener{
    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.fav){
            if(mSharedPrefManager.loginStatus!!) {
                AddToFav(subCategorymodels.get(position).id!!)
            }else{

                    CommonUtil.makeToast(mContext,getString(R.string.login_first))

            }
        }else{
            val intent = Intent(this,ProductActivity::class.java)
            intent.putExtra("id",subCategorymodels.get(position).id)
            startActivity(intent)

        }

    }

    override val layoutResource: Int
        get() = R.layout.activity_subcategory
    lateinit var title: TextView
    lateinit var back: ImageView
    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null

    internal var layNoItem: RelativeLayout? = null

    internal var tvNoContent: TextView? = null

    var swipeRefresh: SwipeRefreshLayout? = null
    lateinit var gridLayoutManager: GridLayoutManager
    var subCategorymodels=ArrayList<SubCategorymodel>()
    lateinit var subCategoryAdapter: SubCategoryAdapter
    lateinit var categoryModel:CategoryModel
    override fun initializeComponents() {
        categoryModel = intent.getSerializableExtra("category") as CategoryModel
        title = findViewById(R.id.title)
        back = findViewById(R.id.back)
        rv_recycle = findViewById(R.id.rv_recycle)
        layNoInternet = findViewById(R.id.lay_no_internet)
        layNoItem = findViewById(R.id.lay_no_item)
        tvNoContent = findViewById(R.id.tv_no_content)
        swipeRefresh = findViewById(R.id.swipe_refresh)
        title.text = categoryModel.name
        back.setOnClickListener { onBackPressed() }
        gridLayoutManager = GridLayoutManager(mContext,2)
        subCategoryAdapter = SubCategoryAdapter(mContext,subCategorymodels,R.layout.recycler_subcategory)
        subCategoryAdapter.setOnItemClickListener(this)
        rv_recycle.layoutManager = gridLayoutManager
        rv_recycle.adapter = subCategoryAdapter
        swipeRefresh!!.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener {
            getData()

        }
        getData()
    }

    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.SubCategory(mLanguagePrefManager.appLanguage,categoryModel.id!!)?.enqueue(object :
            Callback<SubCategoryResponse> {
            override fun onFailure(call: Call<SubCategoryResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<SubCategoryResponse>,
                response: Response<SubCategoryResponse>
            ) {
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
                if (response.isSuccessful) {
                    if (response.body()?.value.equals("1")) {
                        Log.e("myJobs", Gson().toJson(response.body()!!.data))
                        if (response.body()!!.data?.isEmpty()!!) {
                            layNoItem!!.visibility = View.VISIBLE
                            layNoInternet!!.visibility = View.GONE
                            tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)

                        } else {
//
                            subCategoryAdapter.updateAll(response.body()!!.data!!)
                        }
//
                    }else {
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }

                }
            }
        })
    }

    fun AddToFav(id:Int){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.AddFav(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,id)
            ?.enqueue(object :Callback<FavResponse>{
                override fun onFailure(call: Call<FavResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<FavResponse>,
                    response: Response<FavResponse>
                ) {
                   hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            getData()
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

            })
    }
}