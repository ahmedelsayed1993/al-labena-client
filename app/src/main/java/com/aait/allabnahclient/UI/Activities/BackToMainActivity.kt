package com.aait.allabnahclient.UI.Activities

import android.content.Intent
import android.widget.Button
import com.aait.allabnahclient.Base.Parent_Activity
import com.aait.allabnahclient.R

class BackToMainActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_back_to_main
    lateinit var back:Button

    override fun initializeComponents() {
        back = findViewById(R.id.back)
        back.setOnClickListener { startActivity(Intent(this,MainActivity::class.java))
        finish()}

    }

    override fun onBackPressed() {
        super.onBackPressed()
        startActivity(Intent(this,MainActivity::class.java))
        finish()
    }
}