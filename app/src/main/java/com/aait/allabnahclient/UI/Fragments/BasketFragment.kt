package com.aait.allabnahclient.UI.Fragments

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.Nullable
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.allabnahclient.Base.BaseFragment
import com.aait.allabnahclient.Cart.AllCartViewModel
import com.aait.allabnahclient.Cart.CartDataBase
import com.aait.allabnahclient.Cart.CartModelOffline
import com.aait.allabnahclient.Client
import com.aait.allabnahclient.Listeners.OnItemClickListener
import com.aait.allabnahclient.Models.BrickModel
import com.aait.allabnahclient.Models.CoastResponse
import com.aait.allabnahclient.Network.Service
import com.aait.allabnahclient.R
import com.aait.allabnahclient.UI.Activities.CompleteDeliveryActivity
import com.aait.allabnahclient.UI.Adapters.CartAdapter
import com.aait.allabnahclient.Uitls.CommonUtil
import com.bumptech.glide.Glide
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList

class BasketFragment:BaseFragment(),OnItemClickListener {
    override fun onItemClick(view: View, position: Int) {
        if(view.id == R.id.delete) {
            cartOffline = CartModelOffline(
                cartAdapter.data.get(position).product_id,
                cartAdapter.data.get(position).product_name,
                cartAdapter.data.get(position).product_image,
                cartAdapter.data.get(position).total,
                cartAdapter.data.get(position).color,
                cartAdapter.data.get(position).color_id,
                cartAdapter.data.get(position).brick_num,
                cartAdapter.data.get(position).car_num
                ,
                cartAdapter.data.get(position).price
            )
            Log.e("car",Gson().toJson(cartOffline))
           // cartAdapter.Delete(position)
            allCartViewModel!!.deleteItem(cartOffline)


        }

    }

    override val layoutResource: Int
        get() = R.layout.fragment_basket
    companion object {
        fun newInstance(): BasketFragment {
            val args = Bundle()
            val fragment = BasketFragment()
            fragment.setArguments(args)
            return fragment
        }
    }
    lateinit var back:ImageView
    lateinit var notify:ImageView
    lateinit var title:TextView
    lateinit var complete:Button
    var brickModels = ArrayList<BrickModel>()
    lateinit var cartOffline:CartModelOffline
    private var allCartViewModel: AllCartViewModel? = null
    internal lateinit var cartModels: LiveData<List<CartModelOffline>>
    internal lateinit var cartDataBase: CartDataBase
    internal var cartModelOfflines: List<CartModelOffline> = ArrayList()
    internal lateinit var cartAdapter:CartAdapter
    var cartModel = ArrayList<CartModelOffline>()
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var products:RecyclerView
    lateinit var lay:LinearLayout
    lateinit var lay_no:LinearLayout
    lateinit var delivery_before_tax:TextView
    lateinit var cars_num:TextView
    lateinit var transportation_cast:TextView
    lateinit var total_before_tax:TextView
    lateinit var added_tax:TextView
    lateinit var total_after_tax:TextView
    lateinit var tax_value:TextView
    var count = 0
    var delivery = 0
    var tax = 0
    var delivery_price = 0
    var total = 0
    var final_total = 0
    override fun initializeComponents(view: View) {
        getData()
        brickModels.clear()
        cartDataBase = CartDataBase.getDataBase(mContext!!)
        allCartViewModel = ViewModelProviders.of(this).get(AllCartViewModel::class.java)
        back = view.findViewById(R.id.back)
        products = view.findViewById(R.id.products)
        notify = view.findViewById(R.id.notify)
        title = view.findViewById(R.id.title)
        complete = view.findViewById(R.id.complete)
        lay = view.findViewById(R.id.lay)
        lay_no = view.findViewById(R.id.lay_no)
        delivery_before_tax = view.findViewById(R.id.delivery_before_tax)
        cars_num = view.findViewById(R.id.cars_num)
        transportation_cast = view.findViewById(R.id.transportation_cast)
        total_before_tax = view.findViewById(R.id.total_before_tax)
        added_tax = view.findViewById(R.id.added_tax)
        total_after_tax = view.findViewById(R.id.total_after_tax)
        tax_value = view.findViewById(R.id.tax_value)
        title.text = getString(R.string.basket)
        back.visibility = View.GONE
        notify.visibility = View.GONE
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        cartAdapter = CartAdapter(mContext!!,cartModel,R.layout.recycler_basket)
        cartAdapter.setOnItemClickListener(this)
        products.layoutManager = linearLayoutManager
        products.adapter = cartAdapter
        cartModels = allCartViewModel!!.allCart
        allCartViewModel = ViewModelProviders.of(this).get(AllCartViewModel::class.java)
        allCartViewModel!!.allCart.observe(this, object : Observer<List<CartModelOffline>> {
            override fun onChanged(@Nullable cartModels: List<CartModelOffline>) {
                cartModelOfflines = cartModels
                if(cartModels.size==0){
                    lay.visibility = View.GONE
                    lay_no.visibility = View.VISIBLE
                }else {
                    lay.visibility = View.VISIBLE
                    lay_no.visibility = View.GONE

                    cartAdapter.updateAll(cartModels)
                    Log.e("carts", Gson().toJson(cartModels))
                    count = 0
                    var price = 0
                    for (i in 0..cartModels?.size - 1) {
                        count = count + cartModels.get(i)?.car_num!!.toInt()
                        price = price+cartModels.get(i).price!!.toInt()
                        brickModels.add(
                            BrickModel(
                                cartModels.get(i)?.product_id,
                                cartModels.get(i).total!!.toInt(),
                                cartModels.get(i)?.brick_num,
                                cartModels.get(i)?.color_id
                            )
                        )

                        delivery_before_tax.text = (count*delivery).toString()+getString(R.string.rs)
                        total_before_tax.text = price.toString() + getString(R.string.rs)
                        cars_num.text = count.toString()
                        transportation_cast.text = (count*delivery).toString()+getString(R.string.rs)
                        total_before_tax.text = (price +(count*delivery)).toString()+getString(R.string.rs)
                        tax_value.text = getString(R.string.added_tax)+tax.toString()+"%"
                        added_tax.text =  (((price +(count*delivery))*tax)/100).toString()+getString(R.string.rs)
                        total_after_tax.text = (((price +(count*delivery))+(((price +(count*delivery))*tax)/100))).toString()+ getString(R.string.rs)
                        delivery_price = count*delivery
                        total = price +(count*delivery)
                        final_total = ((price +(count*delivery))+(((price +(count*delivery))*tax)/100))




                    }
//                    allCartViewModel!!.deleteItems(cartModels)
//                    CartDataBase.closeDataBase()
                    Log.e("model", count.toString())
                    Log.e("model", Gson().toJson(brickModels))

                }


            }
        })

        complete.setOnClickListener {
            val intent = Intent(activity,CompleteDeliveryActivity::class.java)
            intent.putExtra("order",brickModels)
            intent.putExtra("num",count)
            intent.putExtra("tax",tax)
            intent.putExtra("delivery_price",delivery_price)
            intent.putExtra("total",total)
            intent.putExtra("final_total",final_total)
            startActivity(intent) }

    }

    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Coast()?.enqueue(object: Callback<CoastResponse>{
            override fun onFailure(call: Call<CoastResponse>, t: Throwable) {
                CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<CoastResponse>, response: Response<CoastResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if(response.body()?.value.equals("1")){
                        delivery = response.body()?.data?.delivery_price!!.toInt()
                        tax = response.body()?.data?.tax!!.toInt()
                    }
                }
            }

        })

    }


}


