package com.aait.allabnahclient.UI.Fragments

import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.allabnahclient.Base.BaseFragment
import com.aait.allabnahclient.Client
import com.aait.allabnahclient.Listeners.OnItemClickListener
import com.aait.allabnahclient.Models.OrderModel
import com.aait.allabnahclient.Models.OrdersResponse
//import com.aait.allabnahclient.Models.OrderModel
//import com.aait.allabnahclient.Models.OrderResponse
import com.aait.allabnahclient.Network.Service
import com.aait.allabnahclient.R
import com.aait.allabnahclient.UI.Adapters.OrdersAdapter
//import com.aait.allabnahclient.UI.Activities.OrderDetailsActivity
//import com.aait.allabnahclient.UI.Adapters.OrdersAdapter
import com.aait.allabnahclient.Uitls.CommonUtil
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FinishedFragment:BaseFragment() , OnItemClickListener {
    override fun onItemClick(view: View, position: Int) {

    }

    override val layoutResource: Int
        get() = R.layout.fragment_order
    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null

    internal var layNoItem: RelativeLayout? = null

    internal var tvNoContent: TextView? = null

    var swipeRefresh: SwipeRefreshLayout? = null
    lateinit var linearLayoutManager: LinearLayoutManager
    var orderModels=ArrayList<OrderModel>()
    lateinit var ordersAdapter: OrdersAdapter
    override fun initializeComponents(view: View) {
        rv_recycle = view.findViewById(R.id.rv_recycle)
        layNoInternet = view.findViewById(R.id.lay_no_internet)
        layNoItem = view.findViewById(R.id.lay_no_item)
        tvNoContent = view.findViewById(R.id.tv_no_content)
        swipeRefresh = view.findViewById(R.id.swipe_refresh)
        linearLayoutManager = LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL,false)
        ordersAdapter = OrdersAdapter(mContext!!,orderModels,R.layout.recycler_order)
        ordersAdapter.setOnItemClickListener(this)
        rv_recycle.layoutManager = linearLayoutManager
        rv_recycle.adapter = ordersAdapter

        swipeRefresh!!.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener {
            getData()

        }
        getData()

    }
    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.Orders(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,"completed")?.enqueue(object :
            Callback<OrdersResponse> {
            override fun onFailure(call: Call<OrdersResponse>, t: Throwable) {
                CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
            }

            override fun onResponse(call: Call<OrdersResponse>, response: Response<OrdersResponse>) {
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
                if (response.isSuccessful) {
                    if (response.body()?.value.equals("1")) {
                        Log.e("myJobs", Gson().toJson(response.body()!!.data))
                        if (response.body()!!.data?.isEmpty()!!) {
                            layNoItem!!.visibility = View.VISIBLE
                            layNoInternet!!.visibility = View.GONE
                            tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)

                        } else {
//
                            ordersAdapter.updateAll(response.body()!!.data!!)
                        }
//
                    }else {
                        CommonUtil.makeToast(mContext!!,response.body()?.msg!!)
                    }

                }
            }

        })
    }
}