package com.aait.allabnahclient.UI.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.aait.allabnahclient.Base.ParentRecyclerAdapter
import com.aait.allabnahclient.Base.ParentRecyclerViewHolder
import com.aait.allabnahclient.Cart.CartModelOffline
import com.aait.allabnahclient.Models.CategoryModel
import com.aait.allabnahclient.R
import com.bumptech.glide.Glide

class CartAdapter (context: Context, data: MutableList<CartModelOffline>, layoutId: Int) :
    ParentRecyclerAdapter<CartModelOffline>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val questionModel = data.get(position)
        viewHolder.code!!.setText(mcontext.getString(R.string.code)+ questionModel.product_id+" #")
        Glide.with(mcontext).load(questionModel.product_image).into(viewHolder.image)
        viewHolder.name.text = questionModel.product_name
        viewHolder.total.text = questionModel.total
        if (questionModel.color.equals("")){
            viewHolder.num.visibility = View.VISIBLE
            viewHolder.color.visibility = View.GONE
            viewHolder.black_num.text = questionModel.brick_num
        }else{
            viewHolder.num.visibility = View.GONE
            viewHolder.color.visibility = View.VISIBLE
            viewHolder.the_color.text = questionModel.color
        }
        viewHolder.price_before_vit.text = questionModel.price+mcontext.getString(R.string.rs)

        viewHolder.delete.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)

        })




    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {





        internal var code=itemView.findViewById<TextView>(R.id.code)
        internal var name = itemView.findViewById<TextView>(R.id.name)
        internal var total = itemView.findViewById<TextView>(R.id.total)
        internal var num = itemView.findViewById<LinearLayout>(R.id.num)
        internal var black_num = itemView.findViewById<TextView>(R.id.black_num)
        internal var color = itemView.findViewById<LinearLayout>(R.id.color)
        internal var the_color = itemView.findViewById<TextView>(R.id.the_color)
        internal var price_before_vit = itemView.findViewById<TextView>(R.id.price_before_vit)
        internal var image = itemView.findViewById<ImageView>(R.id.image)
        internal var delete = itemView.findViewById<LinearLayout>(R.id.delete)





    }
}