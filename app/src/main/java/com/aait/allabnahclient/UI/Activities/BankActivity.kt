package com.aait.allabnahclient.UI.Activities

import android.content.Intent
import android.os.Build
import android.widget.*
import com.aait.allabnahclient.Base.Parent_Activity
import com.aait.allabnahclient.Cart.CartDataBase
import com.aait.allabnahclient.Client
import com.aait.allabnahclient.Models.BaseResponse
import com.aait.allabnahclient.Network.Service
import com.aait.allabnahclient.R
import com.aait.allabnahclient.Uitls.CommonUtil
import com.aait.allabnahclient.Uitls.PermissionUtils
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.fxn.utility.ImageQuality

import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

class BankActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_bank_transfer
    lateinit var title: TextView
    lateinit var back: ImageView
    lateinit var online: RadioButton
    lateinit var bank: RadioButton
    lateinit var amoun:TextView
    var id = 0
    var amount = 0
    lateinit var account_num:TextView
    lateinit var holder_name:EditText
    lateinit var bank_name:EditText
    lateinit var image:TextView
    lateinit var confirm:Button
    internal var returnValue: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options = Options.init()
        .setRequestCode(100)                                                 //Request code for activity results
        .setCount(1)                                                         //Number of images to restict selection count
        .setFrontfacing(false)                                                //Front Facing camera on start
        .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
        .setPreSelectedUrls(returnValue)                                     //Pre selected Image Urls
        .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
        .setPath("/pix/images")
    private var ImageBasePath: String? = null
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        amount = intent.getIntExtra("amount",0)
        title = findViewById(R.id.title)
        back = findViewById(R.id.back)
        online = findViewById(R.id.online)
        bank = findViewById(R.id.bank)
        amoun = findViewById(R.id.amount)
        account_num = findViewById(R.id.account_num)
        holder_name = findViewById(R.id.holder_name)
        bank_name = findViewById(R.id.bank_name)
        image = findViewById(R.id.image)
        confirm = findViewById(R.id.confirm)
        title.text = getString(R.string.Bank_payment)
        amoun.text = amount.toString()
        back.setOnClickListener { startActivity(Intent(this,MainActivity::class.java))
            finish()}
        online.setOnClickListener { val intent = Intent(this,PayActivity::class.java)
        intent.putExtra("id",id)
        intent.putExtra("amount",amount)
        startActivity(intent)
            finish()
        }

        image.setOnClickListener {
            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            )) {
                    CommonUtil.PrintLogE("Permission not granted")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                            PermissionUtils.IMAGE_PERMISSIONS,
                            400
                        )
                    }
                } else {
                    Pix.start(this, options)
                    CommonUtil.PrintLogE("Permission is granted before")
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23")
                Pix.start(this, options)
            }
        }

        confirm.setOnClickListener {
            if (CommonUtil.checkEditError(holder_name,getString(R.string.enter_account_holder))||
                CommonUtil.checkEditError(bank_name,getString(R.string.enter_bank_name))||
                CommonUtil.checkTextError(image,getString(R.string.attach_image))){
                return@setOnClickListener
            }else{
                Transfer(ImageBasePath!!)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100) {
            returnValue = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

            ImageBasePath = returnValue!![0]


            if (ImageBasePath != null) {

                image.text = getString(R.string.image_attached)
            }
        }
    }
    fun Transfer(path:String){
        showProgressDialog(getString(R.string.please_wait))
        var filePart: MultipartBody.Part? = null
        val ImageFile = File(path)
        val fileBody = RequestBody.create(MediaType.parse("*/*"), ImageFile)
        filePart = MultipartBody.Part.createFormData("image", ImageFile.name, fileBody)
        Client.getClient()?.create(Service::class.java)?.Pay(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,id,1,amount.toString(),bank_name.text.toString()
        ,holder_name.text.toString(),filePart)?.enqueue(object: Callback<BaseResponse> {
            override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>) {
                hideProgressDialog()
                if (response.isSuccessful) {
                    if (response.body()?.value.equals("1")) {
                        val intent = Intent(this@BankActivity,BackToMainActivity::class.java)

                        startActivity(intent)
                        finish()


                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}