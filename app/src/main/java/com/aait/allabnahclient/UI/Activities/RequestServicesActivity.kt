package com.aait.allabnahclient.UI.Activities

import android.content.Intent
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.aait.allabnahclient.Base.Parent_Activity
import com.aait.allabnahclient.Client
import com.aait.allabnahclient.Models.CategoryModel
import com.aait.allabnahclient.Models.TermsResponse
import com.aait.allabnahclient.Network.Service
import com.aait.allabnahclient.R
import com.aait.allabnahclient.Uitls.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RequestServicesActivity : Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_request_services
    lateinit var title: TextView
    lateinit var back: ImageView
    lateinit var topic: EditText
    lateinit var project_details: EditText

    lateinit var send: Button

    var name = ""
    var type = ""
    override fun initializeComponents() {
        name = intent.getStringExtra("name")
        type = intent.getStringExtra("type")
        title = findViewById(R.id.title)
        back = findViewById(R.id.back)
        topic = findViewById(R.id.topic)
        project_details = findViewById(R.id.project_details)

        send = findViewById(R.id.send)
        back.setOnClickListener { onBackPressed()
            finish()}
        title.text = name
        send.setOnClickListener {
            if(CommonUtil.checkEditError(topic,getString(R.string.enter_topic))||
                CommonUtil.checkEditError(project_details,getString(R.string.enter_project_details)) ){
                return@setOnClickListener
            }else{
                if(mSharedPrefManager.loginStatus!!) {
                    Request()
                }else{
                    CommonUtil.makeToast(mContext,getString(R.string.login_first))
                }
            }
        }
    }

    fun Request(){
        showProgressDialog(getString(R.string.please_wait))

        Client.getClient()?.create(Service::class.java)?.RequestService(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,topic.text.toString(),project_details.text.toString()
            ,type,null)?.enqueue(object:
            Callback<TermsResponse> {
            override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<TermsResponse>, response: Response<TermsResponse>) {
                hideProgressDialog()
                if (response.isSuccessful) {
                    if (response.body()?.value.equals("1")) {

                        CommonUtil.makeToast(mContext,response.body()?.data!!)
                        val intent = Intent(this@RequestServicesActivity,BackActivity::class.java)

                        startActivity(intent)
                        finish()


                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}