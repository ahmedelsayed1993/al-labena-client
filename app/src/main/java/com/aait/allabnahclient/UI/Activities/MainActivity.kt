package com.aait.allabnahclient.UI.Activities

import android.graphics.Color
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.Nullable
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.aait.allabnahclient.Base.Parent_Activity
import com.aait.allabnahclient.Cart.AllCartViewModel
import com.aait.allabnahclient.Cart.CartDataBase
import com.aait.allabnahclient.Cart.CartModelOffline
import com.aait.allabnahclient.R
import com.aait.allabnahclient.UI.Fragments.*
import com.aait.allabnahclient.Uitls.CommonUtil
import maes.tech.intentanim.CustomIntent
import java.util.ArrayList

class MainActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_main

    lateinit var home:LinearLayout
    lateinit var home_image:ImageView
    lateinit var home_text:TextView
    lateinit var notification:LinearLayout
    lateinit var notification_image:ImageView
    lateinit var notification_text:TextView
    lateinit var orders:LinearLayout
    lateinit var orders_image:ImageView
    lateinit var orders_text:TextView
    lateinit var my_account:LinearLayout
    lateinit var account_image:ImageView
    lateinit var account_text:TextView
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)

    private var fragmentManager: FragmentManager? = null
    var selected = 1
    private var transaction: FragmentTransaction? = null
    internal lateinit var homeFragment: HomeFragment
    internal lateinit var notificationFragment: NotificationFragment
    internal lateinit var ordersFragment: BasketFragment
    internal lateinit var accountFragment: AccountFragment

    private var allCartViewModel: AllCartViewModel? = null
    internal lateinit var cartModels: LiveData<List<CartModelOffline>>
    internal lateinit var cartDataBase: CartDataBase
    internal var cartModelOfflines: List<CartModelOffline> = ArrayList()
    override fun initializeComponents() {
        CustomIntent.customType(this,"bottom-to-up")
//        cartDataBase = CartDataBase.getDataBase(this)
//        allCartViewModel = ViewModelProviders.of(this).get(AllCartViewModel::class.java)
//        cartModels = allCartViewModel!!.allCart
//
//        allCartViewModel!!.allCart.observe(this, object: Observer<List<CartModelOffline>> {
//            override fun onChanged(t: List<CartModelOffline>?) {
//                cartModelOfflines = t!!
//            }
//
//        })
        homeFragment = HomeFragment.newInstance()
        notificationFragment = NotificationFragment.newInstance()
        ordersFragment = BasketFragment.newInstance()
        accountFragment = AccountFragment.newInstance()
        fragmentManager = getSupportFragmentManager()
        transaction = fragmentManager!!.beginTransaction()
        transaction!!.add(R.id.home_fragment_container, homeFragment)
        transaction!!.add(R.id.home_fragment_container, notificationFragment)
        transaction!!.add(R.id.home_fragment_container,ordersFragment)
        transaction!!.add(R.id.home_fragment_container,accountFragment)
        transaction!!.commit()
        home = findViewById(R.id.home)
        home_image = findViewById(R.id.home_image)
        home_text = findViewById(R.id.home_text)
        notification = findViewById(R.id.notification)
        notification_image = findViewById(R.id.notification_image)
        notification_text = findViewById(R.id.notification_text)
        orders = findViewById(R.id.orders)
        orders_image = findViewById(R.id.orders_image)
        orders_text = findViewById(R.id.orders_text)
        my_account = findViewById(R.id.my_account)
        account_image = findViewById(R.id.account_image)
        account_text = findViewById(R.id.account_text)
        showhome()
        home.setOnClickListener { showhome() }

        notification.setOnClickListener {
            if(mSharedPrefManager.loginStatus!!){
                shownotification()
            }else{
                CommonUtil.makeToast(mContext,getString(R.string.login_first))
            }
             }
        orders.setOnClickListener { showorder() }
        my_account.setOnClickListener { showaccount() }
    }
    fun showhome(){
        selected = 1
        home_image.setImageResource(R.mipmap.home)
        home_text.textColor = Color.parseColor("#CD9504")
        notification_text.textColor = Color.parseColor("#000000")
        orders_text.textColor = Color.parseColor("#000000")
        account_text.textColor = Color.parseColor("#000000")
        notification_image.setImageResource(R.mipmap.notificatio)
        orders_image.setImageResource(R.mipmap.shopping)
        account_image.setImageResource(R.mipmap.use)

        transaction = fragmentManager!!.beginTransaction()
        //        transaction.hide(mMoreFragment);
        //        transaction.hide(mOrdersFragment);
        //        transaction.hide(mFavouriteFragment);
        transaction!!.replace(R.id.home_fragment_container, homeFragment)
        transaction!!.commit()
    }
    fun shownotification(){
        selected = 2
        home_image.setImageResource(R.mipmap.hom)
        home_text.textColor = Color.parseColor("#000000")
        notification_text.textColor = Color.parseColor("#CD9504")
        orders_text.textColor = Color.parseColor("#000000")
        account_text.textColor = Color.parseColor("#000000")
        notification_image.setImageResource(R.mipmap.notification)
        orders_image.setImageResource(R.mipmap.shopping)
        account_image.setImageResource(R.mipmap.use)

        transaction = fragmentManager!!.beginTransaction()
        //        transaction.hide(mMoreFragment);
        //        transaction.hide(mOrdersFragment);
        //        transaction.hide(mFavouriteFragment);
        transaction!!.replace(R.id.home_fragment_container, notificationFragment)
        transaction!!.commit()
    }
    fun showorder(){
        selected = 3
        home_image.setImageResource(R.mipmap.hom)
        home_text.textColor = Color.parseColor("#000000")
        notification_text.textColor = Color.parseColor("#000000")
        orders_text.textColor = Color.parseColor("#CD9504")
        account_text.textColor = Color.parseColor("#000000")
        notification_image.setImageResource(R.mipmap.notificatio)
        orders_image.setImageResource(R.mipmap.shoppi)
        account_image.setImageResource(R.mipmap.use)

        transaction = fragmentManager!!.beginTransaction()
        //        transaction.hide(mMoreFragment);
        //        transaction.hide(mOrdersFragment);
        //        transaction.hide(mFavouriteFragment);
        transaction!!.replace(R.id.home_fragment_container, ordersFragment)
        transaction!!.commit()
    }
    fun showaccount(){
        selected = 3
        home_image.setImageResource(R.mipmap.hom)
        home_text.textColor = Color.parseColor("#000000")
        notification_text.textColor = Color.parseColor("#000000")
        orders_text.textColor = Color.parseColor("#000000")
        account_text.textColor = Color.parseColor("#CD9504")
        notification_image.setImageResource(R.mipmap.notificatio)
        orders_image.setImageResource(R.mipmap.shopping)
        account_image.setImageResource(R.mipmap.user)

        transaction = fragmentManager!!.beginTransaction()
        //        transaction.hide(mMoreFragment);
        //        transaction.hide(mOrdersFragment);
        //        transaction.hide(mFavouriteFragment);
        transaction!!.replace(R.id.home_fragment_container, accountFragment)
        transaction!!.commit()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finishAffinity()
    }
}