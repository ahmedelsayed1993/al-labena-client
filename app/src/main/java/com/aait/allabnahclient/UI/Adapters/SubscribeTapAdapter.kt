package com.aait.allabnahclient.UI.Adapters

import android.content.Context

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.aait.allabnahclient.R
import com.aait.allabnahclient.UI.Fragments.CurrentFragment
import com.aait.allabnahclient.UI.Fragments.FinishedFragment
import com.aait.allabnahclient.UI.Fragments.PendingFragment


class SubscribeTapAdapter(
    private val context: Context,
    fm: FragmentManager
) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return if (position == 0) {
            PendingFragment()
        }else if(position == 1) {
            CurrentFragment()
        }else{
          FinishedFragment()
        }

    }

    override fun getCount(): Int {
        return 3
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return if (position == 0) {
            context.getString(R.string.Pending_payment)
        }else if(position == 1) {
            context.getString(R.string.current_orders)
        }else{
            context.getString(R.string.finished_orders)
        }
    }
}
