package com.aait.allabnahclient.UI.Activities

import android.content.Intent
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.aait.allabnahclient.Base.Parent_Activity
import com.aait.allabnahclient.Client
import com.aait.allabnahclient.Models.BaseResponse
import com.aait.allabnahclient.Models.CategoryModel
import com.aait.allabnahclient.Models.TermsResponse
import com.aait.allabnahclient.Network.Service
import com.aait.allabnahclient.R
import com.aait.allabnahclient.Uitls.CommonUtil
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

class RequestServiceActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_request_service
    lateinit var title:TextView
    lateinit var back:ImageView
    lateinit var topic:EditText
    lateinit var project_details:EditText
    lateinit var details_required:EditText
    lateinit var send:Button

    lateinit var categoryModel: CategoryModel
    override fun initializeComponents() {
        categoryModel = intent.getSerializableExtra("category") as CategoryModel
        title = findViewById(R.id.title)
        back = findViewById(R.id.back)
        topic = findViewById(R.id.topic)
        project_details = findViewById(R.id.project_details)
        details_required = findViewById(R.id.details_required)
        send = findViewById(R.id.send)
        back.setOnClickListener { onBackPressed()
        finish()}
        title.text = categoryModel.name
        send.setOnClickListener {
            if(CommonUtil.checkEditError(topic,getString(R.string.enter_topic))||
                CommonUtil.checkEditError(project_details,getString(R.string.enter_project_details)) ||
                CommonUtil.checkEditError(details_required,getString(R.string.enter_details_required))){
                return@setOnClickListener
            }else{
                if(mSharedPrefManager.loginStatus!!) {
                    Request()
                }else{
                    CommonUtil.makeToast(mContext,getString(R.string.login_first))
                }
            }
        }
    }

    fun Request(){
        showProgressDialog(getString(R.string.please_wait))

        Client.getClient()?.create(Service::class.java)?.RequestService(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,topic.text.toString(),project_details.text.toString()
        ,categoryModel.category_type!!,details_required.text.toString())?.enqueue(object: Callback<TermsResponse> {
            override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<TermsResponse>, response: Response<TermsResponse>) {
                hideProgressDialog()
                if (response.isSuccessful) {
                    if (response.body()?.value.equals("1")) {

                        CommonUtil.makeToast(mContext,response.body()?.data!!)
                        val intent = Intent(this@RequestServiceActivity,BackActivity::class.java)

                        startActivity(intent)
                        finish()


                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}