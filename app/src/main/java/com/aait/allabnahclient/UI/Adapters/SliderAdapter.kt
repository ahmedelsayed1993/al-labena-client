package com.aait.allabnahclient.UI.Adapters

import android.content.Context

import android.view.View

import com.aait.allabnahclient.R
import com.github.islamkhsh.CardSliderAdapter
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.card_image_slider.view.*

class SliderAdapter (context:Context,list : ArrayList<String>) : CardSliderAdapter<String>(list) {
    var list = list
    var context=context
    override fun bindView(position: Int, itemContentView: View, item: String?) {

            Picasso.with(context).load(item).resize(300,300).into(itemContentView.image)
            itemContentView.setOnClickListener {

            }

    }

    override fun getItemContentLayout(position: Int) : Int { return R.layout.card_image_slider }
}