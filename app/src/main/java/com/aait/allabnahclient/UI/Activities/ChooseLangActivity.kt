package com.aait.allabnahclient.UI.Activities

import android.content.Intent
import android.widget.Button
import android.widget.ImageView
import com.aait.allabnahclient.Base.Parent_Activity
import com.aait.allabnahclient.R

class ChooseLangActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_choose_lang
    lateinit var back:ImageView
    lateinit var arabic:Button
    lateinit var english:Button
    lateinit var next:Button
    var lang = "ar"

    override fun initializeComponents() {
        back = findViewById(R.id.back)
        arabic = findViewById(R.id.arabic)
        english = findViewById(R.id.english)
        next = findViewById(R.id.next)
        arabic.setOnClickListener { mLanguagePrefManager.appLanguage = "ar"
            startActivity(Intent(this,LoginActivity::class.java))
            finish() }
        english.setOnClickListener { mLanguagePrefManager.appLanguage = "en"
            startActivity(Intent(this,LoginActivity::class.java))
            finish()}
        next.setOnClickListener { }

    }
}