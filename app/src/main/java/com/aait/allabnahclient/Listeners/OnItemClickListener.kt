package com.aait.allabnahclient.Listeners

import android.view.View

interface OnItemClickListener {
    fun onItemClick(view: View, position: Int)
}
