package com.aait.allabnahclient.Cart


import android.content.Context

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import okhttp3.internal.Internal.instance

@Database(entities = [CartModelOffline::class], version = 1)
abstract class CartDataBase : RoomDatabase() {

    abstract fun dao(): DAO

    companion object {

        private var instance: CartDataBase? = null

       public fun getDataBase(context: Context): CartDataBase {

            if (instance == null)
                instance = Room.databaseBuilder(
                    context.applicationContext,
                    CartDataBase::class.java,
                    "cart.db"
                )
                    .
                        build()
            return instance as CartDataBase
        }

        fun closeDataBase() {
            instance = null
        }


        fun destroyInstance() {
            instance = null
        }
    }
}
