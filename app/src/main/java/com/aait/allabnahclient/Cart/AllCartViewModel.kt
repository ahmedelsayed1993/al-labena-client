package com.aait.allabnahclient.Cart

import android.app.Application

import android.os.AsyncTask

import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData

class AllCartViewModel(application: Application) : AndroidViewModel(application) {
    var cartDataBase: CartDataBase
    internal var dao: DAO
    //   new AllAsynTask().execute();
    var allCart: LiveData<List<CartModelOffline>>
        internal set

    init {
        cartDataBase = CartDataBase.getDataBase(application)
        dao = cartDataBase.dao()
        allCart = dao.allCartItems
        //Log.e("cart", Gson().toJson(allCart))
    }

    fun getCart() {
        AllAsynTask().execute()
    }

    fun updateItem(cartModel: CartModelOffline) {

        UpdateAsyncTask().execute(cartModel)
    }

    inner class UpdateAsyncTask : AsyncTask<CartModelOffline, Void, Void>() {

        override fun doInBackground(vararg cartModels: CartModelOffline): Void? {
            dao.updateItem(cartModels[0])
            return null
        }
    }

    fun deleteItem(cartModel: CartModelOffline) {

        DelteAsyncTask().execute(cartModel)
    }

    fun deleteItems(cartModel: List<CartModelOffline>) {

        DeleteAllAsyncTask().execute(cartModel)
    }

    inner class DelteAsyncTask : AsyncTask<CartModelOffline, Void, Void>() {

        override fun doInBackground(vararg cartModels: CartModelOffline): Void? {
            dao.deleteItem(cartModels[0])
            return null
        }
    }

    inner class DeleteAllAsyncTask : AsyncTask<List<CartModelOffline>, Void, Void>() {


        override fun doInBackground(vararg lists: List<CartModelOffline>): Void? {
            dao.deleteItems(lists[0])
            return null
        }
    }

    inner class AllAsynTask : AsyncTask<Void, Void, Void>() {

        override fun doInBackground(vararg voids: Void): Void? {
            allCart = dao.allCartItems
            return null
        }
    }


}
