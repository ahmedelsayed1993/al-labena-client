package com.aait.allabnahclient.Cart


import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(indices = [Index(value = ["product_id"], unique = true)])
class CartModelOffline(
    var product_id: Int,
    var product_name: String?,
    var product_image: String?,
    var total:String?,
    var color:String?,
    var color_id:String?,
    var brick_num:String?,
    var car_num:String?,
    var price:String?


) {
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0



}
