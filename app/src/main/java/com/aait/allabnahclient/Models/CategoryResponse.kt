package com.aait.allabnahclient.Models

import java.io.Serializable

class CategoryResponse:BaseResponse(),Serializable {
    var sliders:ArrayList<String>?=null
    var data:ArrayList<CategoryModel>?=null
}