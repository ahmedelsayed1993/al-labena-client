package com.aait.allabnahclient.Models

import java.io.Serializable

class SubCategoryDetailsModel:Serializable {
    var images: ArrayList<String>? = null
    var id: Int? = null
    var name: String? = null
    var description: String? = null
    var is_favorite: Int? = null
    var rates: String? = null
    var price: String? = null
    var weight: String? = null
    var minimum: String? = null
    var brick_number: String? = null
    var whatsapp: String? = null
    var colors:ArrayList<ColorModel>?=null
}