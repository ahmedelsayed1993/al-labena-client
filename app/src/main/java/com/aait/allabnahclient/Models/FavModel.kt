package com.aait.allabnahclient.Models

import java.io.Serializable

class FavModel:Serializable {
    var id:Int?=null
    var subcategory_id:Int?=null
    var image:String?=null
    var name:String?=null
    var description:String?=null
}