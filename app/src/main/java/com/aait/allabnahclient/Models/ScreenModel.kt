package com.aait.allabnahclient.Models

import java.io.Serializable

class ScreenModel:Serializable {
    var id:Int?=null
    var name:String?=null
    var title:String?=null
    var content:String?=null
    var image:String?=null
}