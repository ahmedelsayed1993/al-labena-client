package com.aait.allabnahclient.Models

import java.io.Serializable

class CategoryModel : Serializable {
    var id:Int?=null
    var name:String?=null
    var category_type:String?=null
    var image:String?=null
}