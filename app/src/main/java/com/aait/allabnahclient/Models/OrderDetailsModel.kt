package com.aait.allabnahclient.Models

import java.io.Serializable

class OrderDetailsModel:Serializable {
    var image:ArrayList<String>?=null
    var name:String?=null
    var description:String?=null
    var code:Int?=null
    var color:String?=null
    var brick_number:String?=null
    var count:String?=null
    var total:String?=null
    var delivery:String?=null
    var tax:String?=null
    var total_tax:String?=null
    var total_amount:String?=null
}