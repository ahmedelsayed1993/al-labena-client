package com.aait.allabnahclient.Models

import java.io.Serializable

class OrderModel:Serializable {
    var id:Int?=null
    var image:String?=null
    var name:String?=null
    var address:String?=null
    var total:String?=null
}