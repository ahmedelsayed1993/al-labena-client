package com.aait.allabnahclient.Models

import java.io.Serializable

class BrickModel:Serializable {
    var subcategory_id:Int?=null
    var count:Int?=null
    var brick_num:String?=null
    var color_id:String?=null

    constructor(subcategory_id: Int?, count: Int?, brick_num: String?, color_id: String?) {
        this.subcategory_id = subcategory_id
        this.count = count
        this.brick_num = brick_num
        this.color_id = color_id
    }
}