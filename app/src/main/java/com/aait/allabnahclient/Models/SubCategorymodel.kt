package com.aait.allabnahclient.Models

import java.io.Serializable

class SubCategorymodel:Serializable {
    var id:Int?=null
    var name:String?=null
    var description:String?=null
    var is_favorite:Int?=null
    var image:String?=null
}