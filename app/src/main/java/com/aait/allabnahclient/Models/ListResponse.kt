package com.aait.allabnahclient.Models

import java.io.Serializable

class ListResponse:BaseResponse(),Serializable {
    var data:ArrayList<ListModel>?=null
}